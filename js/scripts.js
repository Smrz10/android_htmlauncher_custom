	var search_text;
	var  hslider;
	var  sliderv;
	var is_tablet;
	var drawer_search;
	var editMode=0;
	var languageWeather;

		$(document).ready(function(){
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1;
		var body_height=window.innerHeight;
		var body_width=window.innerWidth;
		drawer_search=$(".drawer_search");
			$(".h-size").css("height",(body_height-60)+"px");
			$(".h-size-mid").css("height",(body_height-160)+"px");
			$(".s6-slide, .w-size").css("width",(body_width)+"px").css("padding","30px 0 30px 0");
			search_text=drawer_search.val();
			setTimeout(function(){
				//set th grid
				setGrid('1');
				//init the main slider
				mainSlider();
			//Google search widget
			/*$("#google_now_search").click(function(){
				$(this).val('');
			});*/
			//edition mode
			$("#finish_edition").click(function(){
				saveGrid('1','');
			});
			
			/*$(".gridster li").click(function(){
				$("#divRss").hide();
			});*/
			
			//music player buttons
			$(".music-play").click(function(){
				window.HTMLauncher.playMusic();
				if($(this).hasClass("pause")){
					$(this).removeClass("pause");
				}else{
					$(this).addClass("pause");
				}
			});
			$(".music-next").click(function(){
				window.HTMLauncher.nextMusic();
			});
			$(".music-previous").click(function(){
				window.HTMLauncher.previousMusic();
			});
			//drawer
			drawer_search.click(function(){
			$(this).focus().val('');
			}).keyup(function(){
			$("#drawer_container .swiper-wrapper").hide();
			$("#drawer_search").html($(".drawer").html()).show();
			setClickApp();
				if($(this).val()==""){
					drawerReset('1');
				}else{
					$("#drawer_search .drawer_elements").hide();
					var filter = $(this).val();
					$("#drawer_search div").each(function(){
			            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
			                $(this).fadeOut();
			            } else {
			                $(this).show();
			            }
			        });
				}
			});
			

/*            $('#rssmenu').change(function() {
		        setRSSFeed(this);
	            });*/


			
				//check if it is an android device
				if(isAndroid) {
				getInstalledApps('1');
				window.HTMLauncher.javascriptCallBack();
				}					
				},10);
		});
		
		/*function loadLanguageWeather(clv,htv,lvt,huv,prv,wiv,psv,viv,getv,notv,unav,sv,amv,pmv,dayv,ctv,cqv,cqz,csz,cdz,cvd,cqt,ccz,csn,cqd,cqc,dc,dcs,dct,dqh,dch,dst,dss,dqu,dqq,dqt,das,dqn,tcd,tcc,tch,tco,tcqz,tcds,tcvv,tvt,tvsz,tcvd,ttdz,ttcr,tthu,tcqs,tcr,tcdr,trf,tcde,tcdf,trgf,tre,tcqf,trg,retf,tqsd,rzed,trcz){
			languageWeather =  {cl: clv,ht: htv,lt:lvt,hu:huv,pr:prv,wi: wiv,ps: psv,vi:viv,get: getv,not: notv,una: unav,search: sv,am:amv,pm:pmv,day: dayv,'113': ctv,'114': cqv,'115': cqz,'116': csz,'119': cdz,'122': cvd,'143': cqt,'176': ccz,'179': csn,'182': cqd,'185': cqc,'200': dc,'227': dcs,'230': dct,'248': dqh,'260': dch,'263': dst,'266': dss,'281': dqu,'284': dqq,'293': dqt,'296': das,'299': dqn,'302':tcd,'305':tcc,'308':tch,'311': tco,'314': tcqz,'317':tcds,'320':tcvv,'323': tvt,'326': tvsz,'329': tcvd,'332': ttdz,'335':ttcr,'338': tthu,'350': tcqs,'353':tcr,'356': tcdr,'359': trf,'362': tcde,'365':tcdf,'368': trgf,'371': tre,'374':tcqf,'377': trg,'386': retf,'389': tqsd,'392':rzed,'395': trcz};

		}*/
			
		/*function setRSSFeed(obj) {
				var feedurl = $('option:selected', obj).val();
				
				if (feedurl) {
					$('#rss').rssfeed(feedurl);
				}
			}*/
	
		function sendPref(name,value,init){
				if(!$("#"+name).is("input")){
					$("body").append("<input type=\"hidden\" id=\""+name+"\" value=\""+value+"\"/>");
				}
				init(value);
		}
		
		/*function initWeather(){
			$(".ws-location").html($("#weatherCity").val());
			   

					$('#weatherslider').weatherSlider({
					WWOAPIKey: 'gyvz2sxny73ud4a6hcjm32ux',
					premiumAPIKey: false,
					imgPath: './images/weather/',
					measurement : 'metric', timeFormat : 24, language: languageWeather, daytime : [7,19], windyWeather : 18, icyTemp : -2, slideDelay : 0, refreshInterval : 0, keybNav : true, touchNav : true, hideBackground : true, enableWeatherInfo : true, enableForecast : false, alwaysShowForecast: false, CSSanimations : true, JSanimations : true, infoDuration : 450, showLoc : true, showTime : true, showCond : true, showTemp : true, showLow : true, showHigh : true, showHum : true, showVis : true, showFDay : true, showFCond : true, showFLow : true, showFHigh : true, snow : true, rain : true, wind : true, lightnings : true, windDirection : 'auto'
			});
			setTimeout('date_heure()','1000');
		}*/
		
		function drawerReset(blur){
			$(".drawer .drawer_elements").show();
			drawer_search.val(search_text);
			$("#drawer_search").hide();
			$("#drawer_container .swiper-wrapper").show();
			if(blur=='1'){
				drawer_search.blur();
			}
			 setClickApp();
		}
		
		function loadFont(font){
			if(!$("#loadfont").is("style")){
				$("head").append("<style id=\"loadfont\">@font-face {font-family: "+font+";src: url('file:///android_asset/fonts/"+font+".otf');}body { font-family:"+font+"}</style>");
			}else{
				$("#loadfont").html("@font-face {font-family: "+font+";src: url('file:///android_asset/fonts/"+font+".otf');}body { font-family:"+font+"}");
			}
		}
		
		function loadTheme(theme){
			if(!$("#loadtheme").is("link")){
				$("head").append("<style type=\"text/css\" id=\"loadtheme\">@import url(\"css/themes/"+theme+".css\");</style>");
			}else{
				$("#loadtheme").attr("href","css/"+theme+".css");
			}
			//$("body").prepend("<textarea style=\"width:450px;height:229px;\"><style type=\"text/css\" id=\"loadtheme\">@import url(\"css/"+theme+".css\");</style></textarea>");
		}
		
		//for tablets
		function gridsterTablets(){
			is_tablet=1;
		}
		
		
		function getInstalledApps(populate){
			window.HTMLauncher.getInstalledApps(populate);
			window.HTMLauncher.recentApps();
			setClickApp();
		}
		
		/*function setFacebookLogged(){
			$(".fb-slide-container").prepend(" ");
		}*/
		 
		 function mainSlider(){
		 hslider = $('.content-slider-1').s6({
			dynamicShadow : false,
			onSlideChangeEnd:function(){
					if(hslider.activeSlide=="0"){//homepage slide
								window.HTMLauncher.setMenu('1');
							}else if(hslider.activeSlide=="1"){//drawer
								window.HTMLauncher.setMenu('2');
								if($("#drawer_search").is(":visible")){
									drawerReset('1');
								}
							}else if(hslider.activeSlide=="2"){
								/*if($("#rss").html()==""){
									setRSSFeed('#rssmenu');
								}*/
							}else if(hslider.activeSlide=="3"){
								window.HTMLauncher.setMenu('1');
							}
				/*$(".slide"+hslider.prevIndex+",.slide"+hslider.nextIndex).hide();
				$(".slide"+hslider.activeSlide).show();*/
			}
			});
			
		 }
		 
		 function reload(){
			window.HTMLauncher.reload();
		 }
		 
		 function swiperDrawer(){
			new Swiper('#drawer_container',{
			mode: 'vertical'
				});
			}
		 
		 function verticalSlider(){
			new Swiper('#recent_apps_container',{
			pagination: '.pagination',
			paginationClickable: true,
			mode: 'vertical'
				});
		 }
		
		function goToHome(construct){
			if(hslider.activeSlide!=0){
				hslider.swipePrev();
				goToHome('0');
			}
		}
		
		function goTo(id){
			if(hslider.activeSlide!=id){
				hslider.swipePrev();
				goTo(id);
			}
		}
		
		function goToDrawer(){
			if(hslider.activeSlide!=1){
				hslider.swipeNext();
				goToDrawer();
			}
		}
		
		function populate(div, data) {
		    $(div).append(data);
		}
		
		function clearDiv(div){
			$(div).empty();
		}
		
		function removeDiv(div){
			$(div).remove();
		}
		
		function uninstallApp(){
			goToDrawer();
		    $(".rslider").addClass("rsNoDrag");
		    $("#top_message").show();
			$(".drawer .drawer_uninstall").show();
			$(".drawer img").unbind().click(function(){
				if($(this).hasClass("uninstall")){
				   var rel = $(this).attr("rel");
				   var src = $(this).attr("src");
				   window.HTMLauncher.uninstallApp(rel);
				   }
			});
		}
		
		function removeIcon(package_name){
			 $(".gridster ."+package_name).remove();
		}

		function editGrid() {
			   setGrid('0');
			   $(".gridster").addClass("gridster-edition").removeClass("gridster-saved");
			   var id =hslider.activeSlide;
			   hslider.destroy();
			   $("#top_message").show();
			   $(".gridster img").unbind();
			   $(".gridster li").each(function(){
			   var li = $(this);
				   $(li).click(function(){
						$(".drawer .drawer_add").show();
							goToDrawer();
							$(".drawer img").unbind().click(function(){
								goTo(id);
								var src = $(this).attr("src");
								var rel = $(this).attr("rel");
								var classt=rel.replace(/\./g, '');
								  li.html("<img src=\""+src+"\" rel=\""+rel+"\" class=\""+classt+" icon_app\"/>");
							});
				   });
			   });
		}
		
		function saveGrid(edit, force){
			var headtags="";
			var headcss="";
			if($("#top_message").is(":visible") || force=='1'){
				window.HTMLauncher.progressDialog();
				$("#top_message").hide();
				$(".drawer_uninstall").hide();
				$(".drawer_add").hide();
						setGrid('1');
						mainSlider(); 
					 $(".gridster").removeClass("gridster-edition").addClass("gridster-saved");
					 $(".gridster li, .drawer img").unbind().removeClass("gridster-add");
					 setClickApp();
					  if(edit=='1'){
					  $(".gridster li").each(function(){
						var lihtml= $(this).html();
						if(lihtml==""){
							$(this).addClass("invisible");
						}else{
							$(this).removeClass("invisible");
						}
					  });
					  var body = $("body").html();
					  $("#pagebuffer").html(body);
					  //emptiable class to empty dynamic container
					  var city=$(".ws-location").html();
					  $("#pagebuffer .emptiable").empty();
					  $(".head-tag-js").each(function(){
						headtags = headtags+"<script type=\"text/javascript\" src=\""+$(this).attr("src")+"\"  class=\"head-tag-js\"></script>\n";
					  });
					  $(".head-tag-css").each(function(){
						headcss = headcss+"<link rel=\"stylesheet\" href=\""+$(this).attr("href")+"\" type=\"text/css\" media=\"screen\" class=\"head-tag-css\"/>\n";
					  });
					  var font = $("#loadfont").html();
					  $("#pagebuffer .s6-container,#pagebuffer .s6-wrapper,#pagebuffer .swiper-wrapper,#pagebuffer .s6-slide,#pagebuffer .fb-slide-container,#pagebuffer #top_message,#pagebuffer .gridster ul,#pagebuffer .gridster li,#pagebuffer .gridster").removeAttr("data-s6rotatex").removeAttr("data-s6rotatex").removeAttr("data-s6rotatey").removeAttr("data-s6rotatez").removeAttr("style");
					 $("#pagebuffer #weatherslider").html("<span class=\"ws-location\">"+city+"</span>");
					  var body_clean = $("#pagebuffer").html();
					  $("#pagebuffer").html("");
					 window.HTMLauncher.cancelProgressDialog();
					 window.HTMLauncher.updateHTML("","index.html","<html id=\"wholepage\">\n<head>\n<meta content=\"minimum-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no\" name=\"viewport\"/>\n"+headcss+"\n"+headtags+"\n<style id=\"loadfont\">\n"+font+"\n</style>\n</head>\n<body>\n"+body_clean+"\n</body>\n</html>",false);
					 }
			}
		}
		
		function reset(){
				setGrid('1');
				mainSlider();
				window.HTMLauncher.javascriptCallBack();
				getInstalledApps('1');
				window.HTMLauncher.recentApps();
		}
		
		function setClickRecentApp(){
			$("#recent-wrapper img").unbind().click(function(){
					var package_name=$(this).attr("rel");
					window.HTMLauncher.launchRemoteApp(package_name);
			});
		}

		function setClickApp(){
			$(".drawer img, .gridster img, #drawer_search div img").unbind().click(function(){
			if($(this).parent(".drawer_elements").is("div") && drawer_search.val() !=search_text){
				drawerReset('1');
			}
					var package_name=$(this).attr("rel");
					
					if($(this).attr("class")=="developer"){
						window.HTMLauncher.developerShortcut();
					}
					
					if($(this).attr("class")=="settings"){
						window.HTMLauncher.settingsShortcut();
					}
					
					if(package_name!=undefined){
						window.HTMLauncher.launchRemoteApp(package_name);
						window.HTMLauncher.recentApps();
					}
			});
			
			
	
			$(".gridster.gridster-saved li").longpress(function(e) {
					if(!$(this).is(':empty')){
							var rel = $(this).children("img").attr("rel");
							window.HTMLauncher.menuApp(rel);
					}
			});
		}
		
		/*function initWidgetTime(){
			var heure   = now.getHours();
			var minute  = now.getMinutes();
			var seconde = now.getSeconds();
			var result = heure+':'+minute+':'+seconde;
			$(".ws-time").html(result);
			setTimeout('initWidgetTime()','60000');
		}*/
		
		function date_heure(){
		date = new Date;
		h = date.getHours();
		if(h<10)
		{
		h = "0"+h;
		}
		m = date.getMinutes();
		if(m<10)
		{
		m = "0"+m;
		}
		s = date.getSeconds();
		if(s<10)
		{
		s = "0"+s;
		}
		resultat = ''+h+':'+m;
		$(".ws-time").html(resultat);
		setTimeout('date_heure();','1000');
		return true;
		}
		
		
		function setGrid(set){
		 	var gridster;
	        $(function(){
		        gridster = $(".gridster ul").gridster({
		          max_cols:5,
		          max_row:8,
		          widget_base_dimensions: [73, 73],
		          widget_margins: [4, 4],
		        }).data('gridster').disable();
		     });
	       if(set==0){
		       gridster.enable();
	       }
		}
		
